extends TextureRect

enum DecoType {STONE, TREE, FLOWER, GRASS}

# --- Configuration
const TYPE_IMAGE  = {
	DecoType.STONE: "res://images/board/DecoStone.png",
	DecoType.TREE: "res://images/board/DecoTree.png",
	DecoType.FLOWER: "res://images/board/DecoFlower.png",
	DecoType.GRASS: "res://images/board/DecoGrass.png"
}

const NB_DECO = {
	DecoType.STONE: 50,
	DecoType.TREE: 30,
	DecoType.FLOWER: 100,
	DecoType.GRASS: 200
}

# For usage : considere using "deco_chart_size_or_default()" instead.
# It handle default value for unspecified element.
const DECO_CHART_SIZE = {
	DecoType.STONE: Vector2(192.0, 192.0),
	DecoType.FLOWER: Vector2(96.0, 96.0),
	DecoType.GRASS: Vector2(64.0, 64.0)
}

const DECO_MIN_X_COORD = -6000
const DECO_MAX_X_COORD = 10000
const DECO_MIN_Y_COORD = -6000
const DECO_MAX_Y_COORD = 6000
# --- End Configuration

func _init(type):
	texture = load(TYPE_IMAGE[type])
	mouse_filter = Control.MOUSE_FILTER_IGNORE
	
func _ready():
	pass
	
# If value not specified, will take default (256x256)
static func deco_chart_size_or_default(type):
	var size = Vector2(256.0, 256.0)
	if DECO_CHART_SIZE.has(type):
		size = DECO_CHART_SIZE[type]
	return size
