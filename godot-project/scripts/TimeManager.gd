extends Node2D

class_name TimeManager

# Signals
signal time_changed
signal state_changed
signal end_of_day

# Consts
const DAY_DURATION_MS := 120 * 1000

# Variables
var game_time_ms := 0
var time_state = Globals.TimeState.PAUSED
var current_day := 1

func _ready():
	emit_signal('state_changed', time_state)

func _process(delta):
	var time_factor := Globals.time_state_to_time_factor(time_state)
	
	game_time_ms += int(delta * 1000 * time_factor)
	var day = game_time_ms / DAY_DURATION_MS + 1
	var progression = float((game_time_ms % DAY_DURATION_MS)) / float(DAY_DURATION_MS)
	emit_signal('time_changed', game_time_ms, progression, day)
	
	if day > current_day:
		current_day = day
		time_state = Globals.TimeState.PAUSED
		emit_signal('end_of_day', day)
		emit_signal('state_changed', time_state)
		
func add_day(nb = 1, from_start = true):
	game_time_ms += DAY_DURATION_MS * nb
	if from_start:
		game_time_ms = current_day * DAY_DURATION_MS
		
	current_day += nb
	
	var progression = float((game_time_ms % DAY_DURATION_MS)) / float(DAY_DURATION_MS)
	emit_signal('time_changed', game_time_ms, progression, current_day)

func _on_next_state():
	if time_state == Globals.TimeState.PAUSED:
		time_state = Globals.TimeState.RUNNING
	elif time_state == Globals.TimeState.RUNNING:
		time_state = Globals.TimeState.FAST
	elif time_state == Globals.TimeState.FAST:
		time_state = Globals.TimeState.PAUSED
	emit_signal('state_changed', time_state)

func set_state(time_state):
	if time_state == Globals.TimeState.PAUSED:
		_on_pause()
	elif time_state == Globals.TimeState.RUNNING:
		_on_normal()
	elif time_state == Globals.TimeState.FAST:
		_on_fast()
	else:
		print_debug("Unhandled time state")
		pass
		
	emit_signal('state_changed', time_state)
	
func _on_pause():
	time_state = Globals.TimeState.PAUSED

func _on_fast():
	time_state = Globals.TimeState.FAST

func _on_normal():
	time_state = Globals.TimeState.RUNNING

func _on_Game_day_changed():
	time_state = Globals.TimeState.RUNNING
	emit_signal('state_changed', time_state)
	
func get_current_day():
	return current_day
