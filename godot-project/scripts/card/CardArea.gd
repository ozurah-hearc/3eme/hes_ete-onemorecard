extends Area2D

# Parent node containing the "card.gd" script
onready var parent := get_parent()
onready var collision_shape := $CollisionShape2D

func _ready():
	pass

func _on_mouse_entered():
	parent.is_being_hovered = true

func _on_mouse_exited():
	parent.is_being_hovered = false
