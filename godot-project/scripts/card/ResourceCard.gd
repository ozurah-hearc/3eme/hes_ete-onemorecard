extends "res://scripts/card/Card.gd"

export var quantity := 1

func _ready():
	self.background_color = Color(0.4, 0.7, 1.0)
