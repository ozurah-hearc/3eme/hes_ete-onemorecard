extends Node2D
class_name Card

# Script Consts
const Stack := preload("res://scripts/Stack.gd")

# Consts
const BACKGROUND_IMAGE := preload("res://images/Card.png")
const HALF_WIDTH = 86.0
const HALF_HEIGHT = 120.0

const SELL_LOOKUP_WIDTH = 300
const SELL_LOOKUP_HEIGHT = 800

# Scene variables
onready var root := $"/root"
onready var game := root.get_node("Game")
onready var board = game.get_node("Board")
onready var card_area := $Card
onready var sprite_background := $Card/SpriteBackground
onready var sprite_icon := $Card/SpriteIcon
onready var title := $Card/Title
onready var stat_left_wrapper := $Card/StatLeft
onready var stat_right_wrapper := $Card/StatRight
onready var stat_left_value := $Card/StatLeft/Value
onready var stat_right_value := $Card/StatRight/Value
onready var shop_area = board.get_node("TopInteraction/SellArea")

# Script variable
export var card_name := ""
export var label := ""
export var sell_value := -1

# Variables
var parent_stack = null
var is_being_hovered := false
var is_already_used_in_craft := false
var background_color := Color(1.0, 0.0, 0.0) setget set_bg_color, get_bg_color

func _ready():
	title.text = label
	var filename = card_name.capitalize()
	var icon = load("res://images/card-pictures/%s.png" % filename)
	sprite_icon.texture = icon
	var material = sprite_background.get_material().duplicate()
	sprite_background.set_material(material)
	material.set_shader_param("tex", BACKGROUND_IMAGE)
	update_stats()

func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if not event.pressed:
				game.current_dragged_node = null

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				on_card_mousedown()
			else:
				on_card_mouseup()
				handle_card_selling()
		return
	
	if event is InputEventMouseMotion:
		if game.current_dragged_node == self \
			or parent_stack and game.current_dragged_node == parent_stack:
			update_position()

func on_card_mousedown():
	if not is_being_hovered:
		return
		
	if parent_stack:
		parent_stack.split(self)
		
	var node = parent_stack if parent_stack else self
	game.current_dragged_node = node
	game.relative_dragged_position = get_global_mouse_position() - node.position
	node.raise()
	update_position()
	get_tree().set_input_as_handled()

func on_card_mouseup():
	if game.is_camera_dragged:
		pass
	
	var node = parent_stack if parent_stack else self
	
	if node.position.y < 300.0:
		node.position.y = 300.0
	
	if not is_being_hovered:
		return
	
	# Directly handle click release if we don't have any card to drop
	if not game.current_dragged_node:
		get_tree().set_input_as_handled()
		return
	
	# Ignore when a card is dropped on "itself"
	if is_card_being_dragged():
		return
	
	if parent_stack:
		stack_nodes(game.current_dragged_node, parent_stack)
	else:
		stack_nodes(game.current_dragged_node, self)
	
	game.current_dragged_node = null
	get_tree().set_input_as_handled()

func delete():
	if parent_stack:
		parent_stack.unstack_card(self)
	
	game.remove_card(self) # To ensure the card is removed from the game and the HUD is updated
	queue_free() # ... because this one is called asynchronus

func is_card_being_dragged():
	if game.current_dragged_node == null:
		return false
		
	return game.current_dragged_node == self \
		or game.current_dragged_node == parent_stack

func handle_card_selling():
	if self.parent_stack:
		return
		
	if card_area.overlaps_area(shop_area):
		if self.sell_value == -1:
			return
			
		if self.sell_value > 0:
			var maybe_stack = null
			
			for card in game.get_all_cards():
				if card.card_name == 'coin':
					var card_x = card.parent_stack.global_position.x if card.parent_stack else card.global_position.x
					var card_y = card.parent_stack.global_position.y if card.parent_stack else card.global_position.y
					if abs(card_x - shop_area.global_position.x) < SELL_LOOKUP_WIDTH \
						and abs(card_y - shop_area.global_position.y) < SELL_LOOKUP_HEIGHT:
							maybe_stack = card.parent_stack if card.parent_stack else card
							break

			var new_cards = []
			for i in range(0, sell_value):
				var new_card = game.create_card("coin", self.position, false)
				maybe_stack = new_card if maybe_stack == null else stack_nodes(new_card, maybe_stack)
				new_cards.append(new_card)
		
		game.remove_card(self) # To ensure the card is removed from the game and the HUD is updated
		queue_free() # ... because this one is called asynchronus

func update_position():
	var node = parent_stack if parent_stack else self
	node.position = get_global_mouse_position() - game.relative_dragged_position

	replace_inside_board()

func update_stats():
	var left_stat = get_left_stat()
	var right_stat = get_right_stat()
	stat_left_wrapper.visible = left_stat > 0
	stat_right_wrapper.visible = right_stat > 0
	stat_left_value.text = str(left_stat)
	stat_right_value.text = str(right_stat)

func get_left_stat():
	return sell_value

func get_right_stat():
	return 0

# Replace the card/stack inside the board if needed
func replace_inside_board():
	var node = parent_stack if parent_stack else self
	var bottom_height = HALF_HEIGHT
	if node is Stack:
		bottom_height += (node.cards.get_child_count() - 1) \
		* node.Y_SPACE_BETWEEN_CARDS
	
	var pos_left = node.position.x - HALF_WIDTH
	var pos_right = node.position.x + HALF_WIDTH
	var pos_top = node.position.y - HALF_HEIGHT
	var pos_bottom = node.position.y + bottom_height
	
	# Replace the card/stack inside the board if needed
	if pos_left < board.margin_left:
		node.position.x = board.margin_left + HALF_WIDTH
	elif pos_right > board.margin_right:
		node.position.x = board.margin_right - HALF_WIDTH
	if pos_top < board.margin_top:
		node.position.y = board.margin_top + HALF_HEIGHT
	elif pos_bottom > board.margin_bottom:
		node.position.y = board.margin_bottom - bottom_height

# Stack one card/stack with another card/stack
static func stack_nodes(node1, node2):
	if node1 is Stack:
		if node2 is Stack:
			# Stack dropped on stack
			node2.concat_stack(node1)
			return node2
		else:
			# Stack dropped on card
			node1.prepend_card(node2)
			return node1
	else:
		if node2 is Stack:
			# Card dropped on stack
			node2.push_card(node1)
			return node2
		else:
			# Card dropped on card
			return Stack.push_card_on_card(node1, node2)

func set_bg_color(color):
	background_color = color
	var material = sprite_background.get_material()
	material.set_shader_param("color", background_color)

func get_bg_color():
	return background_color
