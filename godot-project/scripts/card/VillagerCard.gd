extends "res://scripts/card/Card.gd"

const HUNGER_VALUE_TO_FEED := 2

func _ready():
	self.background_color = Color(0.4, 1.0, 0.7)

func get_left_stat():
	return 0
