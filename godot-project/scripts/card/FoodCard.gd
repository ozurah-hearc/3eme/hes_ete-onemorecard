extends "res://scripts/card/Card.gd"

export var hunger_value := 1

func _ready():
	self.background_color = Color(1.0, 0.7, 0.4)

func get_right_stat():
	return hunger_value

static func sort(card1, card2):
	return card1.hunger_value < card2.hunger_value
