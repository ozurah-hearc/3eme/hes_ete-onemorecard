extends Node

onready var root := $"/root"
onready var game := root.get_node("Game")
onready var board = game.get_node("Board")

onready var debug_hud = self

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _add_card(name):
	game.create_card(name, Vector2(0,500), true)
	
func _on_ButtonClose_pressed():
	debug_hud.visible = false

func _on_ButtonAddApple_pressed():
	_add_card("apple")

func _on_ButtonAddBerry_pressed():
	_add_card("berry")

func _on_ButtonAddWood_pressed():
	_add_card("wood")

func _on_ButtonAddStone_pressed():
	_add_card("stone")

func _on_ButtonAddBush_pressed():
	_add_card("bush")

func _on_ButtonAddTree_pressed():
	_add_card("tree")

func _on_ButtonAddRock_pressed():
	_add_card("rock")

func _on_ButtonAddVillager_pressed():
	_add_card("villager")

func _on_ButtonAddGrave_pressed():
	_add_card("grave")

func _on_ButtonAddHouse_pressed():
	_add_card("house")

func _on_ButtonAddShed_pressed():
	_add_card("shed")

func _on_ButtonAddSoil_pressed():
	_add_card("soil")

func _on_ButtonAddCoin_pressed():
	_add_card("coin")
