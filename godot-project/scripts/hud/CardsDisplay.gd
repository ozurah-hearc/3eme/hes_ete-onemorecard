extends Label

func _ready():
	pass # Replace with function body.
	
func _on_Game_cards_changed(nb_cards, max_cards):
	self.text = '%02d/%02d' % [int(nb_cards), int(max_cards)]
	_change_color(nb_cards > max_cards)
	
func _change_color(is_colored):
	var color = Color(0.5,0,0,1) if is_colored else Color(1,1,1,1)
	self.add_color_override("font_color", color)
