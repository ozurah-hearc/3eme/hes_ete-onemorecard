extends Area2D
class_name BuyDeck

# Script Consts
const CoinCard = preload("res://scripts/card/CoinCard.gd")
const VillagerCard = preload("res://scripts/card/VillagerCard.gd")

# Scene variables
onready var root := $"/root"
onready var game := root.get_node("Game")
onready var buy_deck_area := self
onready var collision_shape := $CollisionShape2D
onready var label := $ColorRect/Label
onready var price := $ColorRect/Price

# Script variable (setted in scene)
onready var deck_proba_repartition = []

export var deck_name: String = 'Unamed'
export var cost: int = 5
export var deck_size: int = 5
export var content = []

# Variables
var rng = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	label.text = deck_name
	price.text = '%d coins' % cost
	for deck_dic in content:
		for j in range(0, deck_dic.get('weight')):
			deck_proba_repartition.append(deck_dic.get('card'))

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and not event.pressed:
			var coin_cards = []
			for overlap_card in game.get_all_cards():
				if overlap_card is CoinCard and self.overlaps_area(overlap_card.get_node('Card')):
					var stack: Stack = overlap_card.parent_stack
					if not stack: #  to avoid error with non-stack elements
						continue

					# ensure stack is enough cards and only coins
					var stack_cards = stack.cards.get_children()
					if stack_cards.size() >= cost:
						var only_coins = true
						for card in stack_cards:
							if not card is CoinCard:
								only_coins = false
								break
						if not only_coins:
							continue
							
						var stack_size = stack.cards.get_child_count()
						
						# delete as many coins of the stack as the deck cost
						var ref_position = null
						for i in range(0, cost):
							var card = stack.get_top_card()
							if not ref_position:
								ref_position = card.global_position
							stack.unstack_card(card, false)
							card.delete()
						stack.update_content()
						
						# Generate weighted random cards according the deck
						var maybe_stack = null
						for i in range(0, deck_size):
							var picked_card = deck_proba_repartition[rng.randi_range(0, deck_proba_repartition.size() - 1)]
							var card = game.create_card(picked_card, ref_position + Vector2(0, 50), false)
							maybe_stack = card if maybe_stack == null else Card.stack_nodes(card, maybe_stack)
						
						# For deck which cost at least 10, a villager card is created
						# if there is only 1 villager on the board
						var villager_count = 0
						for card in game.get_all_cards():
							if card is VillagerCard:
								villager_count += 1
						if villager_count == 1 and cost >= 10:
							var card = game.create_card("villager", ref_position + Vector2(0, 50), false)
					break
