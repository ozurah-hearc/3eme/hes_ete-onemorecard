extends Label

func _ready():
	pass # Replace with function body.
	
func _on_TimeManager_time_changed(game_time_ms, progression, day):
	var hours = progression * 24
	var minutes = (hours - floor(hours)) * 60
	self.text = 'day %02d, %02d:%02d' % [day, int(hours), int(minutes)]
