extends Label

func _ready():
	pass # Replace with function body.
	
func _on_Game_food_changed(nb_food, required_food):
	self.text = '%02d/%02d' % [int(nb_food), int(required_food)]
	_change_color(nb_food < required_food)
	
func _change_color(is_colored):
	var color = Color(0.5,0,0,1) if is_colored else Color(1,1,1,1)
	self.add_color_override("font_color", color)
