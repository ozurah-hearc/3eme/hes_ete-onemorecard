extends HBoxContainer

onready var next_day_button := $NextDayButton

func _on_TimeManager_end_of_day(day):
	next_day_button.text = "Go to day %d" % day
	visible = true

func _on_Game_day_changed():
	visible = false

func _on_Game_cards_changed(having, needing):
	next_day_button.disabled = having > needing
