extends Node2D
class_name Stack

# Script Consts
var ResourceCard = load("res://scripts/card/ResourceCard.gd")

# Consts
const SCENE_PATH = "res://objects/Stack.tscn"
const Y_SPACE_BETWEEN_CARDS = 48.0

# Scene variables
onready var root = $"/root"
onready var game = $"/root/Game"
onready var cards = $Cards
onready var gauge_border = $GaugeBorder
onready var gauge = $GaugeBorder/Control/Gauge

# Variables
var current_craft = null
var craft_progression := 0.0

# Used to add cards to a freshly created stack,
# since the stack doesn't contains its childen nodes yet
var future_cards = []

func _ready():
	var is_first_card := true
	for card in future_cards:
		if is_first_card:
			position = card.position
			is_first_card = false
		push_card(card)

func _process(delta):
	# Craft processing
	var time_factor := Globals.time_state_to_time_factor(game.time_state)
	if current_craft:
		craft_progression += current_craft.speed * time_factor * delta
		if craft_progression >= 1.0:
			end_craft()
		gauge.anchor_right = craft_progression

func push_card(card):
	card.parent_stack = self
	card.get_parent().remove_child(card)
	cards.add_child(card)
	if cards.get_child_count() > 1:
		update_content()

func prepend_card(card):
	card.parent_stack = self
	card.get_parent().remove_child(card)
	cards.add_child(card)
	cards.move_child(card, 0)
	position = card.position
	update_content()

func pop_card():
	return unstack_card_at(cards.get_child_count() - 1)

func unstack_card_at(index, should_content_be_updated = true):
	var to_unstack = cards.get_child(index)
	unstack_card(to_unstack, should_content_be_updated)
	return to_unstack

func unstack_card(card, should_content_be_updated = true):
	var pos = card.global_position
	card.parent_stack = null
	cards.remove_child(card)
	game.cards.add_child(card)
	card.position = pos
	raise()
	if should_content_be_updated:
		update_content()

# Split the stack into two stacks,
# the given card is the first card of the new stack
func split(card):
	if card == get_bottom_card():
		return
	if card == get_top_card():
		pop_card()
		return
	var pos = card.global_position
	var stack = load(SCENE_PATH).instance()
	while true:
		var poped_card = pop_card()
		stack.future_cards.append(poped_card)
		if card == poped_card:
			break
	stack.future_cards.invert()
	game.cards.add_child(stack)
	return stack

func concat_stack(other_stack):
	for card in other_stack.cards.get_children():
		push_card(card)
	other_stack.queue_free()

func get_top_card():
	if cards.get_child_count() == 0:
		return null
	return cards.get_child(cards.get_child_count() - 1)

func get_bottom_card():
	if cards.get_child_count() == 0:
		return null
	return cards.get_child(0)

func update_content():
	# If it's no more a stack
	if cards.get_child_count() <= 1:
		if cards.get_child_count() == 1:
			unstack_card(cards.get_child(0), false)
		queue_free()
		return
	
	# Place card on the stack
	var y := 0.0
	for card in cards.get_children():
		card.position = Vector2(0.0, y)
		y += Y_SPACE_BETWEEN_CARDS
	
	# Repositionning stack in the board
	var top_card = get_top_card()
	if top_card:
		top_card.replace_inside_board()
	
	# Perform craft
	cancel_craft()
	current_craft = get_available_craft()
	if current_craft:
		start_craft()

func get_available_craft():
	if cards.get_child_count() < 2:
		return null
	
	var stack_names := []
	for card in cards.get_children():
		stack_names.append(card.card_name)
	stack_names.sort()
	
	for craft in game.crafts:
		if stack_names == craft.materials_and_workforce:
			return craft
	
	return null

func start_craft():
	gauge_border.visible = true

func end_craft():
	if not current_craft:
		return
	
	for card in cards.get_children():
		card.is_already_used_in_craft = false
	
	var materials = current_craft.materials.duplicate()
	
	while not materials.empty():
		var card_name = materials.pop_back()
		for card in cards.get_children():
			if card.is_already_used_in_craft or card_name != card.card_name:
				continue
			card.is_already_used_in_craft = true
			if card is ResourceCard:
				card.quantity -= 1
				if card.quantity <= 0:
					unstack_card(card, false)
					card.delete()
			else:
				unstack_card(card, false)
				card.delete()
	
	for card_name in current_craft.result:
		game.create_card(card_name, position, true)
	
	gauge_border.visible = false
	current_craft = null
	craft_progression = 0.0
	update_content()

func cancel_craft():
	gauge_border.visible = false
	craft_progression = 0.0

static func push_card_on_card(card1, card2) -> Stack:
	var stack = load(SCENE_PATH).instance()
	stack.future_cards.append(card2)
	stack.future_cards.append(card1)
	card1.get_node("/root/Game/Cards").add_child(stack)
	return stack
