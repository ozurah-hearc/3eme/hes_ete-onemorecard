extends Node

var ENABLE_DEBUG_FUNCTION = bool(OS.get_environment("DEBUG"))

func _ready():
	pass

enum TimeState {PAUSED, RUNNING, FAST}

func time_state_to_time_factor(time_state) -> float:
	match time_state:
		Globals.TimeState.PAUSED:
			return 0.0
		Globals.TimeState.RUNNING:
			return 1.0
		Globals.TimeState.FAST:
			return 4.0
		_:
			assert(false, 'Invalid time state')
	return 0.0
