extends HBoxContainer

func _on_TimeManager_end_of_day(day):
	visible = false

func _on_Game_day_changed():
	visible = true
