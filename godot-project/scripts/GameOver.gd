extends Control

signal day_setted

func _ready():
	pass
	
func _on_ButtonGoHome_pressed():
	SceneSwitcher.goto_scene("res://scenes/MainMenu.tscn")
	
func display(nb_day):
	visible = true
	emit_signal("day_setted", nb_day)
