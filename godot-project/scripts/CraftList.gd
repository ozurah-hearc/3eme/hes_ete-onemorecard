extends Node

onready var button_open = $ButtonOpen
onready var paper_craft = $List

func _ready():
	pass # Replace with function body.

func _on_ButtonOpen_pressed():
	visibility(true)

func _on_ButtonClose_pressed():
	visibility(false)

func visibility(is_open):
	button_open.visible = !is_open
	paper_craft.visible = is_open
