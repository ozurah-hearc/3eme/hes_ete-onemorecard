extends Object

# Craft progression done in one second
var speed := 0.0
var materials := []
var workforce := []
var materials_and_workforce := []
var result := []

func _init(duration, mats, works, res):
	speed = 1.0 / duration
	materials = mats
	workforce = works
	result = res
	materials_and_workforce = materials.duplicate()
	materials_and_workforce.append_array(workforce)
	materials_and_workforce.sort()
	result.sort()

func _ready():
	pass
