extends Node2D

# Signals
signal coin_changed
signal food_changed
signal cards_changed
signal day_changed

# Consts
const CARDS_PATH := "res://objects/cards/%s.tscn"
const INITIAL_CARD_ALLOWED := 20


const ZOOM_LEVEL_MULTIPLIER = 1.125
const ZOOM_LEVEL_MULTIPLIER_STARTUP = 1.5
const ZOOM_MIN_LEVEL = 0.5
const ZOOM_MAX_LEVEL = 4.0
const BOARD_POS_Y_STARTUP = 395
const BOARD_SIZE_INCR_X = 64.0
const BOARD_SIZE_INCR_Y = 80.0

# Script Consts
const Card = preload("res://scripts/card/Card.gd")
const VillagerCard = preload("res://scripts/card/VillagerCard.gd")
const BuildingCard = preload("res://scripts/card/BuildingCard.gd")
const ResourceCard = preload("res://scripts/card/ResourceCard.gd")
const FoodCard = preload("res://scripts/card/FoodCard.gd")
const CoinCard = preload("res://scripts/card/CoinCard.gd")

const Craft = preload("res://scripts/Craft.gd")

const BoardDeco = preload("res://scripts/BoardDeco.gd")

# Scene variables
onready var root = $"/root"
onready var cards = $Cards
onready var camera = $Camera2D
onready var board = $Board
onready var board_decoration = $Board/Decoration

onready var gameover = $Camera2D/GameOverHUD/GameOver

onready var time_manager = $TimeManager

onready var debug_hud = $Camera2D/DebugHUD
onready var debug_board = $DebugBoard

# Variables
var cards_prototype := {}
var crafts := []

var is_camera_dragged := false
var camera_drag_position := Vector2()

var current_dragged_node = null
var relative_dragged_position = Vector2()

var nb_max_card := 20
var time_state = Globals.TimeState.PAUSED

func _ready():
	debug_board.visible = Globals.ENABLE_DEBUG_FUNCTION
	
	randomize()
	init_all_cards_prototype()
	init_all_crafts()
	
	var start_cards = ["villager", "bush", "tree"]
	var center = board.get_position() + Vector2(board.get_size().x /2, board.get_size().y /2)
	for start_card in start_cards:
		var d = 2.0 * Vector2(rand_range(-60.0, 60.0), rand_range(-60.0, 60.0))
		create_card(start_card, center + d)

	create_all_decos()
	
	# Camera position init
	camera.zoom *= ZOOM_LEVEL_MULTIPLIER * ZOOM_LEVEL_MULTIPLIER_STARTUP
	# camera.position.x is centered -> OK
	camera.position.y = BOARD_POS_Y_STARTUP

func _input(event):
	if event is InputEventMouseButton:
		# Board drag enable
		if event.button_index == BUTTON_LEFT:
			is_camera_dragged = event.pressed
			camera_drag_position = get_viewport().get_mouse_position()
		
		# Board zoom
		elif event.button_index == BUTTON_WHEEL_UP:
			zoom_camera()
		elif event.button_index == BUTTON_WHEEL_DOWN:
			unzoom_camera()
		return
	
	if event is InputEventMouseMotion:
		# Board drag move
		if is_camera_dragged:
			var new_position := get_viewport().get_mouse_position()
			var delta_position := camera_drag_position - new_position
			camera.position += camera.zoom * delta_position
			camera_drag_position = new_position
			camera.position.x = clamp(
				camera.position.x,
				board.margin_left,
				board.margin_right
			)
			camera.position.y = clamp(
				camera.position.y,
				board.margin_top,
				board.margin_bottom
			)
		return

func _physics_process(delta):
	pass

func _process(delta):
	
	# %%%%%%%%%%%%%%%%%%%%%%%  DEBUG + test keys %%%%%%%%%%%%%%%%%%%%%%%
	if not Globals.ENABLE_DEBUG_FUNCTION:
		return
		
	if Input.is_action_just_pressed("debug_add_card"):
		debug_hud.visible = true
	
	if Input.is_action_just_pressed("debug_feed_villager"):
		feed_villagers()
	
	if Input.is_action_just_pressed("debug_add_day"):
		time_manager.add_day()
		start_new_day()
		
	if Input.is_action_just_pressed("debug_update_hud"):	
		update_hud_cards()
		update_hud_coins()
		update_hud_food()
		
	if Input.is_action_just_pressed("debug_board_expension"):
		increase_board_size()
		
	if Input.is_action_just_pressed("debug_do_gameover"):
		perform_game_over()

func zoom_camera():
	camera.zoom /= ZOOM_LEVEL_MULTIPLIER
	if camera.zoom.x < ZOOM_MIN_LEVEL:
		camera.zoom = Vector2(ZOOM_MIN_LEVEL,ZOOM_MIN_LEVEL)

func unzoom_camera():
	camera.zoom *= ZOOM_LEVEL_MULTIPLIER
	if camera.zoom.x > ZOOM_MAX_LEVEL:
		camera.zoom = Vector2(ZOOM_MAX_LEVEL, ZOOM_MAX_LEVEL)

func increase_board_size():
	board.margin_left -= BOARD_SIZE_INCR_X
	board.margin_right += BOARD_SIZE_INCR_X
	board.margin_bottom += BOARD_SIZE_INCR_Y

func init_all_cards_prototype():
	# Type: VillagerCard
	init_card_prototype("villager")
	
	# Type: BuildingCard
	init_card_prototype("house")
	init_card_prototype("soil")
	init_card_prototype("shed")
	
	# Type: ResourceCard
	init_card_prototype("wood")
	init_card_prototype("stone")
	init_card_prototype("bush")
	init_card_prototype("grave")
	init_card_prototype("rock")
	init_card_prototype("tree")
	
	# Type: FoodCard
	init_card_prototype("berry")
	init_card_prototype("apple")
	
	# Type: Coin
	init_card_prototype("coin")


func init_card_prototype(name):
	var filename = name.capitalize()
	cards_prototype[name] = load(CARDS_PATH % filename)

func init_all_crafts():
	init_craft(60.0, ["stone", "wood", "wood"], ["villager"], ["house"])
	init_craft(10.0, ["bush"], ["villager"], ["berry"])
	init_craft(10.0, ["rock"], ["villager"], ["stone"])
	init_craft(10.0, ["tree"], ["villager"], ["wood", "apple"])
	init_craft(20.0, ["apple"], ["soil"], ["tree"])
	init_craft(20.0, ["berry"], ["soil"], ["bush"])
	init_craft(30.0, ["wood","wood", "stone", "stone"], ["villager"], ["shed"])
	init_craft(60.0, [], ["house", "villager", "villager"], ["villager"])

func init_craft(duration, materials, workforce, result):
	crafts.append(Craft.new(duration, materials, workforce, result))

func create_card(name, position, is_position_randomized = false):
	if is_position_randomized:
		var angle = rand_range(0, TAU)
		var dist = rand_range(64.0, 128.0)
		position.x += dist * cos(angle)
		position.y += dist * sin(angle)
	var card = cards_prototype[name].instance()
	card.position = position
	cards.add_child(card)
	card.replace_inside_board()
	
	update_hud()
	
	return card

func start_new_day():
	feed_villagers()
	
	if get_nb_villager() <= 0:
		perform_game_over()
		return
	
	update_hud()
	
	increase_board_size()
	
	emit_signal('day_changed')

func feed_villagers():
	var cards = get_all_cards()
	
	var villager_cards := []
	var food_cards := []
	
	for card in cards:
		if card is VillagerCard:
			villager_cards.append(card)
		elif card is FoodCard:
			food_cards.append(card)
	
	food_cards.sort_custom(FoodCard, "sort")
	
	for villager_card in villager_cards:
		var remaining_value_to_feed := VillagerCard.HUNGER_VALUE_TO_FEED
		while remaining_value_to_feed > 0:
			if food_cards.empty():
				kill_villager(villager_card)
				break
			var food_card = food_cards.pop_back()
			remaining_value_to_feed -= food_card.hunger_value
			food_card.delete()

func kill_villager(villager_card):
	var pos = villager_card.global_position
	var is_position_randomized = not not villager_card.parent_stack
	villager_card.delete()
	create_card("grave", pos, is_position_randomized)

func get_all_cards():
	var list := []
	for child in cards.get_children():
		if child is Card:
			list.append(child)
			continue
		if not child is Stack:
			continue
		for card in child.cards.get_children():
			list.append(card)
	return list

func remove_card(card):
	cards.remove_child(card)
	update_hud()

# Create and place all decorations on the board according BoardDeco.gd configuration
func create_all_decos():
	for type in range(BoardDeco.NB_DECO.size()):
		for nb in range(BoardDeco.NB_DECO[type]):
			var size = BoardDeco.deco_chart_size_or_default(type)
			create_deco(BoardDeco.NB_DECO.keys()[type], size, Vector2(0.0, 0.0), true)

# Create and place 1 decoration on the board according BoardDeco.gd configuration
func create_deco(deco_type, size, position, is_position_randomized = false):
	if is_position_randomized:
		var pos_x = rand_range(BoardDeco.DECO_MIN_X_COORD, BoardDeco.DECO_MAX_X_COORD)
		var pos_y = rand_range(BoardDeco.DECO_MIN_Y_COORD, BoardDeco.DECO_MAX_Y_COORD)
		position.x += pos_x
		position.y += pos_y
	
	var deco = BoardDeco.new(deco_type)	

	deco.expand = true # To enabled resizing
	deco.set_size(size)
	
	# Should be setted after size, else it override the size with
	# the image default size if it too small
	deco.rect_position = position
	
	board_decoration.add_child(deco)

# --- For HUD Interaction

func get_nb_coin():
	var sum = 0
	
	for card in get_all_cards():
		if card is CoinCard:
			sum += 1
			
	return sum
	
func get_nb_food():
	var food_value = 0
	
	for card in get_all_cards():
		if card is FoodCard:
			food_value += (card as FoodCard).get_right_stat()
			
	return food_value
	
func get_required_food():
	return get_nb_villager() * VillagerCard.HUNGER_VALUE_TO_FEED
	
func get_nb_card():
	var cards = get_all_cards()
	var sum = cards.size()
	
	for card in cards:
		if card is CoinCard:
			sum -= 1
			
	return sum
	
func get_max_nb_card():
	var shed_bonus = 0
	for card in get_all_cards():
		if card.card_name == 'shed':
			shed_bonus += 4
	
	return INITIAL_CARD_ALLOWED + shed_bonus

func update_hud():
	update_hud_cards()
	update_hud_coins()
	update_hud_food()
# --- End HUD Interaction

func get_nb_villager():
	var cards = get_all_cards()
	var sum = 0
	
	for card in cards:
		if card is VillagerCard:
			sum += 1
			
	return sum

# --- Events signal for HUD

func update_hud_coins():
	var nb_coin = get_nb_coin()
	emit_signal('coin_changed', nb_coin)
	
func update_hud_cards():
	var having = get_nb_card()
	var needing = get_max_nb_card()
	emit_signal('cards_changed', having, needing)
	
func update_hud_food():
	var having = get_nb_food()
	var needing = get_required_food()
	emit_signal('food_changed', having, needing)

# --- End Events signal for HUD

func perform_game_over():
	time_manager.set_state(Globals.TimeState.PAUSED)
	gameover.display(time_manager.get_current_day() -1)

func _on_NextDayButton_pressed():
	if get_nb_card() <= get_max_nb_card():
		start_new_day()
		
func _on_TimeManager_state_changed(new_time_state):
	time_state = new_time_state
