# Gr 7 OneMoreCard

![Logo](https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3281-projet-p3-hes-ete/gr-7-onemorecard/-/raw/main/document/GIMP/export/Logo.png)

[Accès au wiki](https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3281-projet-p3-hes-ete/gr-7-onemorecard/-/wikis/home)

[Accès au projet (Godot)](https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3281-projet-p3-hes-ete/gr-7-onemorecard/-/tree/main/godot-project)

[Accès aux images (GIMP)](https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3281-projet-p3-hes-ete/gr-7-onemorecard/-/tree/main/document/GIMP)

[Accès aux images (exportées)](https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3281-projet-p3-hes-ete/gr-7-onemorecard/-/tree/main/document/GIMP/export)
